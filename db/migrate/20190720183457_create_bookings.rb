class CreateBookings < ActiveRecord::Migration[5.2]
  def change
    create_table :bookings do |t|
    #person info
      t.integer  :status, default: 0
      t.string  :title
    	t.string  :person_name
      t.string  :valid_telephone_number
      t.string  :email
    #person current address
      t.string  :current_house_name_or_number
      t.string  :current_street_name
      t.string  :current_post_code

    #person new address
      t.string  :new_house_name_or_number
      t.string  :new_street_name
      t.string  :new_post_code

    #when moving will be happend?
      t.string   :expected_removal_day
     
      t.timestamps
    end
  end
end
