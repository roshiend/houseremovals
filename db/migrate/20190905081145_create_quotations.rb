class CreateQuotations < ActiveRecord::Migration[5.2]
  def change
    create_table :quotations do |t|
      t.references  :booking, foreign_key: true
      t.string :status
      t.monetize :price
      t.string :customer_name
      t.string :from_address
      t.string :to_address

      t.string :vechile_type
      t.string :team_members
      t.string :house_flat_type

      t.timestamps
    end
  end
end
