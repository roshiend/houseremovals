class CreateRemovableItems < ActiveRecord::Migration[5.2]
  def change
    create_table :removable_items do |t|
      t.string :item_name
      t.timestamps
    end
  end
end
