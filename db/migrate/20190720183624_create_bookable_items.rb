class CreateBookableItems < ActiveRecord::Migration[5.2]
  def change
    create_table :bookable_items do |t|
      t.references 	:booking, foreign_key: true
      t.references 	:removable_item, foreign_key: true
      t.integer 	:bookable_item_qty
      t.timestamps
    end
  end
end
