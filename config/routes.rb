Rails.application.routes.draw do
devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  namespace :admin do
  	resources :email, only: [:new, :create]
  end
 	get "terms" => 'page#terms', :as => :terms
	root 'page#index'
	resources :page, only: [:index, :new, :create,:terms]
	#resources :bookings, only: [:new, :create]

  match "*path", to: "errors#not_found", via: :all
    match "*path", to: "errors#internal_server_error", via: :all
end
