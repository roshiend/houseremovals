class Booking < ApplicationRecord
	has_many :bookable_items, dependent: :destroy, class_name: 'BookableItem'
	has_many :removable_items, through: :bookable_items
	

  has_one :quotation
  
	accepts_nested_attributes_for :bookable_items, :allow_destroy => true
  accepts_nested_attributes_for :quotation, :allow_destroy => false

  #after_create :set_status

	enum status: {issue_a_quote: 0,sent_and_pending: 1,accepted: 2}

#https://stackoverflow.com/questions/9174513/rails-has-many-through-form-with-checkboxes-and-extra-field-in-the-join-model

	def initialized_removable_items # this is the key method
    [].tap do |o|
      RemovableItem.all.each do |removable_item|
        if c = bookable_items.find { |c| c.removable_item_id == removable_item.id }
          o << c.tap { |c| c.enable ||= true }
        else
          o << BookableItem.new(removable_item: removable_item)
        end
      end
    end
  end

  def quotations
     Quotation.where(booking_id: id)
  end
  

end
