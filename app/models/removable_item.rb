class RemovableItem < ApplicationRecord
  has_many :bookable_items
  has_many :bookings, through: :bookable_items
end
