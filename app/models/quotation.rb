class Quotation < ApplicationRecord
	belongs_to :booking
	enum status: [ :sent_and_pending,:accepted,:issue_a_quote ]

	monetize :price_cents,:numericality => {:greater_than => 0}
end
