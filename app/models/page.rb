class Page < MailForm::Base

 

	attribute :name,      :validate => true
  attribute :phone,      :validate => true
  attribute :post_code,      :validate => true
  attribute :email,     :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i

  attribute :message
  attribute :nickname,  :captcha  => true


  def headers
    {
      :subject => "Free quotation Request",
      :to => "roshiend@gmail.com",
      :from => %("#{name}" <#{email}>)
    }
  end
 
end


