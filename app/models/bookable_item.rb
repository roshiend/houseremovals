class BookableItem < ApplicationRecord
	belongs_to :booking
	belongs_to :removable_item

	attr_accessor :enable
	
end
