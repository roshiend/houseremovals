ActiveAdmin.register Quotation do

	belongs_to :booking
	permit_params :booking_id,:customer_name,:from_address,
	:to_address,:vechile_type,:team_members,:house_flat_type,:price,:Price_cents

	form :partial => "admin/bookings/quotations/form"

	after_action :update_booking_status, only: [:create]
	controller do
		def find_resource
	     @booking = Booking.find(params[:booking_id])
	     @quotation = Quotation.find(params[:id])
	    end

	    def show
	      super do |format|
	      	format html
	        format.pdf { render(pdf: "quotation-#{resource.id}.pdf") }
	      end
	    end

	    def update_booking_status
	    	@booking.update_attributes(status: :sent_and_pending)

	    end
	end


	show do |q|
		panel "Quotation Preview" do
	     table_for quotation do 
	     	
	#         row "Booking Id" do 
	#           quotation.booking_id
	#         end
	#         row "customer" do 
	#           quotation.customer_name
	#         end
	#         row "Moving From" do 
	#           quotation.from_address
	#         end
	#          row "Moving to" do 
	#           quotation.to_address
	#         end
	#          row "Required Vechile" do 
	#           quotation.vechile_type
	#         end
	#          row "Number of team members" do 
	#           quotation.team_members
	#         end
	#         row "house Type" do 
	#           quotation.house_flat_type
	#         end
		
	    end
	end
	end
end
