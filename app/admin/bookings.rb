ActiveAdmin.register Booking do
		permit_params :status,:title,:person_name,:email,:valid_telephone_number,:current_house_name_or_number,
		:current_street_name,:current_post_code,
		:new_house_name_or_number,:new_street_name,:new_post_code,:expected_removal_day,
		bookable_items_attributes:[:id,:bookable_item_qty,:removable_item_id,:booking_id, :_destroy,:enable]

		before_action :process_bookable_item_attrs, only: [:create, :update]
		form :partial => "form"
		

		controller do
			def process_bookable_item_attrs
			    params[:booking][:bookable_items_attributes].values.each do |cat_attr|
			      cat_attr[:_destroy] = true if cat_attr[:enable] != '1'
			    end
			end

			def find_resource
				@booking = Booking.find(params[:id])
			end
			


			
		end

		show :title => proc {|b| "Booking details for "+ b.title + " "+ b.person_name } do 
			panel "Contact Info", :class =>"contact-info-booking-admin" do
				attributes_table_for booking  do
					row :title
					row :person_name
					row "Phone" do |booking|
						booking.valid_telephone_number
					end
					row :email
				end
				panel "Current Address", :class =>"address-current-panel-admin" do
					attributes_table_for booking do
						row :current_house_name_or_number
						row :current_street_name
						row :current_post_code
					end
				end

				panel "Moving to...", :class =>"address-current-panel-admin" do
					attributes_table_for booking do
						row :new_house_name_or_number
						row :new_street_name
						row :new_post_code
					end
				end
				
			end

			panel "I want to Move these...", :class =>"contact-info-booking-want-to-move-admin" do
				table_for booking.bookable_items do 
					column "" do |bi|
        		bi.removable_item.item_name
      		end
      		column "" do 
      			"x"
      		end
      		column "" do |bi|
      			bi.bookable_item_qty
      		end
				end
			end

			panel "Removable Day ", :class =>"contact-info-booking-want-to-move-admin" do
				table_for booking do 
					column "" do  |b|
						b.expected_removal_day
					end
				end
      		end

      		panel "Quotations", :class =>"contact-info-booking-want-to-move-admin" do
      			table_for booking do  |q|
					column "" do  
						# if booking.issue_a_quote!
						# 	link_to "issue a Quote", new_admin_booking_quotation_path(b),:class=>"issue_qute-btn-issue"
						# elsif booking.sent_and_pending!
						# 	"sent_and_pending"
						# else  booking.accepted!
						# 	"accepted"
							
						# end

						if booking.sent_and_pending?
							link_to "View Quote", admin_booking_quotation_path(booking,booking.quotation.id),:class=>"issue_qute-btn-issue"
						elsif booking.accepted?
							"Booking Accepted"
						else booking.issue_a_quote?
							link_to "issue a Quote", new_admin_booking_quotation_path(booking),:class=>"issue_qute-btn-issue"
						end	
							
						
						

						
					end
				end

      		end
      		
      		
				
      		
		end
			
		index do 
			
			column :title do |b|
				link_to "#{b.title}",admin_booking_path(b.id)
			end
			column :person_name do |b|
				link_to "#{b.person_name}",admin_booking_path(b.id)
			end
			column  "Post code",:current_post_code
			column  "Removal Day",:expected_removal_day
			actions
		end



end
