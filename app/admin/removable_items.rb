ActiveAdmin.register RemovableItem do

permit_params :item_name, :default_qty


form do |f|
	f.inputs "Removable Item" do
		f.input :item_name
	end
	f.actions
end

end
