class PageController < ApplicationController
  def index
  	@contact = Page.new(params[:page])
    set_meta_tags og:{
      title: "R D L Removals",
      description:"Your Local House Removing Partner",
      keywords: "near me, house rent, house rent near me, chester, west-midlands, stoke-on-trent, stoke, cheap , affordable, rent a car, house removals chester, house removals stoke-on-trent, house removals, house removals costs, house removal companies, house removals london, house removal boxes, house removal costs uk, man with van, man with van london, manwith man stoke-on-trent, man with van chester,man with van near me, man with van hire, man with van delivery service, man with van courier, man with van courier uk",
      url: "https://www.rdlway.com",
      image: "https://www.rdlway.com/assets/van-69b5822dbf42fd16902a0323fe50f07363ec82af4c67c58c6acfbc115b1ebf31.png",
      type:"website"
    },
    fb:{app_id: "459724851566905"}



    #meta tags added for facebook
    
  end

  def new
  	
  end

  def create
    
  	@contact = Page.new(params[:page])
    @contact.request = request
    respond_to do |format|
      if @contact.deliver
        # re-initialize Home object for cleared form
        @contact = Page.new
        format.html { render 'index'}
        format.js   { flash.now[:success] = @message = "Thank you for your request. One of our team member will contact you soon" }
      #working code uncomment whenever need
      #------------------------------------- 

        # begin
        #  #get predefine message as well as can genereate dynamicly based on requirements
        # TwilioClient.send_message(message)
        #   puts "-- Text Message sent! --"
        # rescue Twilio::REST::TwilioError => error
        #   p error.message
          
        # end

      else
        format.html { render 'page/contact' }
        format.js   { flash.now[:error] = @message = "Something went wrong, Please try again" }
      end
    end
  end

  def terms
    respond_to do |format|
      format.html
      format.js
    end
  end

  private
  def message
    "New Qoutation Request, Please Check your E-mails"
  end

end
