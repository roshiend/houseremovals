class BookingsController < ApplicationController
  before_action :process_bookable_item_attrs, only: [:create, :update]
  def new
  	@booking = Booking.new
   
  end

  def create
    @booking = Booking.new(booking_params)
     	
  	if @booking.save
  		redirect_to root_path
  	else
  		render :new
      
  	end
  end



  def process_bookable_item_attrs
    params[:booking][:bookable_items_attributes].values.each do |cat_attr|
      cat_attr[:_destroy] = true if cat_attr[:enable] != '1'
    end
  end

  private
  	def booking_params
  		params.require(:booking).permit(:person_name,:post_code,
  		:address,:valid_telephone_number,:email,removable_item_ids:[],bookable_items_attributes:[:bookable_item_qty, :booking_id, :removable_item_id, :_destroy,:enable]	)
  	end

end
