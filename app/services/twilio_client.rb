class TwilioClient
  def self.send_message(message)
    new.send_message(message)
  end

  def initialize
    account_sid = ENV['TWILIO_ACCOUNT_SID']
    auth_token  = ENV['TWILIO_AUTH_TOKEN']
    @client = Twilio::REST::Client.new(account_sid, auth_token)
  end

  def send_message(message)
    @client.messages.create(
      from:  twilio_number,
      to:    agent_number,
      body:  message
    )
  end

  private

  def twilio_number
   #registred number with twillio
   twilio_number = ENV['TWILIO_NUMBER']
  end

  def agent_number
    #who do u want to send ?
    agent_number = ENV['AGENT_NUMBER']
  end

  
end

